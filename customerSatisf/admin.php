<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>

table{
    direction:rtl;
}
</style>
</head>
<body>

<?php 
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "customersatisfaction";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
$conn->query("SET NAMES UTF8");
$conn->query("SET CHARACTER SET UTF8");
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
   

session_start();
 
if(!isset($_SESSION["loggedin_admin"]) || $_SESSION["loggedin_admin"] !== true){
    header("location: logout.php");
    exit;}

$sql ="SELECT * FROM customerinfo_v2"; 

if(isset($_GET['cdate']) && ($_GET['ndate'])){
$now = $_GET['cdate']; 

$next=$_GET['ndate'];

$now1=strtotime($now);
$d = date('Y-m-d',$now1);

$now2=strtotime($next);
$n=date('Y-m-d',$now2);
$sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n')"; 
}

$result = $conn->query($sql);

$y=1;


?>


<div class="topnav">
    <a class="active" href="admin.php" >عرض الكل</a>
    <a href="newuser.php" >مستخدم جديد</a>
    <a href="logout.php" class='log_outbuttonn' >تسجيل الخروج</a>
 
</div>


<form name="subSat" action="admin.php" method="get" class="form_subSat">
    <label class="label_form">عرض التقيمات من تاريخ</label>
    <input type="date" name='cdate' class="input_form"/>
    <label class="label_form">الى تاريخ</label>
    <input type="date" name='ndate' class="input_form" />
    <input type="submit" value="طلب" class="button_form"  />
</form>



    <table class='table-margin' >
        <th class="cell100 column1"> التاريخ </th>
        <th class="cell100 column1"> اسم الزبون</th>
        <th class="cell100 column1"> رقم الهاتف</th>
        <th class="cell100 column1">طبيعة العمل </th>
        <th class="cell100 column1">الايميل</th>
        <th class="cell100 column1">تقييم الموقع</th>
        <th class="cell100 column1"> تقييم النظافة  </th>
        <th class="cell100 column1">نقييم الديكور</th>
        <th class="cell100 column1">تقييم الهدوء</th>
        <th class="cell100 column1"> تقييم الخدمة  </th>
        <th class="cell100 column1">تقييم الطعم </th>
        <th class="cell100 column1">تقييم الوصول</th>
        <th class="cell100 column1">تقييم السعر </th>
        <th class="cell100 column1">تقييم الموظفين</th>
        <th class="cell100 column1">تقييم الاستقبال </th>
        <th class="cell100 column1">تقديم اقتراحات </th>

 		
<?php 
    if ($result->num_rows > 0) {
   
    while($row = $result->fetch_assoc()) {
?>

    <tr  >	

	<td > <?php echo $row['date'] ?></td>
    <td > <?php echo $row['customer_name'] ?></td>
    <td > <?php echo $row['customer_phone'] ?></td>
    <td > <?php echo $row['customer_job'] ?></td>	
    <td > <?php echo $row['customer_email'] ?></td>	
                            
    <td > <?php echo $row['location_rate'] ?></td>
    <td > <?php echo $row['clean_rate'] ?></td>
    <td > <?php echo $row['decor_rate'] ?></td>
    <td > <?php echo $row['quite_rate'] ?></td>	
    <td > <?php echo $row['service_rate'] ?></td>
    <td > <?php echo $row['taste_rate'] ?></td>
    <td > <?php echo $row['delivery_rate'] ?></td>
    <td > <?php echo $row['cost_rate'] ?></td>
    <td > <?php echo $row['employee_rate'] ?></td>	
    <td > <?php echo $row['resiption_rate'] ?></td>	

    <td > <?php echo $row['suggestion'] ?></td>	

                
    </tr>
<?php  $y=$y+1;?>

    <?php
 }
 } ?>
</table>

<?php 

$rowcount=mysqli_num_rows($result);
//printf($rowcount);


    if(isset($_GET['cdate']) && ($_GET['ndate'])){
        $now = $_GET['cdate']; 
        $next=$_GET['ndate'];

        $now1=strtotime($now);
        $d = date('Y-m-d',$now1);

        $now2=strtotime($next);
        $n=date('Y-m-d',$now2);


        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n')";
        $result = $conn->query($sql);
        $num=mysqli_num_rows($result);


        if($num == 0){$num=1;}


        /*---------------location rate -----------------*/
        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`location_rate`='1.0'||`location_rate`='1.5') ";
        $result = $conn->query($sql);
        $loc_rate1=mysqli_num_rows($result);
  

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`location_rate`='2.0'||`location_rate`='2.5') ";
        $result = $conn->query($sql);
        $loc_rate2=mysqli_num_rows($result);
      

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`location_rate`='3.0'||`location_rate`='3.5') ";
        $result = $conn->query($sql);
        $loc_rate3=mysqli_num_rows($result);
 

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`location_rate`='4.0'||`location_rate`='4.5') ";
        $result = $conn->query($sql);
        $loc_rate4=mysqli_num_rows($result);
  

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and `location_rate`='5.0' ";
        $result = $conn->query($sql);
        $loc_rate5=mysqli_num_rows($result);



        /*---------------clean rate -----------------*/
        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`clean_rate`='1.0'||`clean_rate`='1.5') ";
        $result = $conn->query($sql);
        $cl_rate1=mysqli_num_rows($result);
    

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`clean_rate`='2.0'||`clean_rate`='2.5') ";
        $result = $conn->query($sql);
        $cl_rate2=mysqli_num_rows($result);
        

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`clean_rate`='3.0'||`clean_rate`='3.5') ";
        $result = $conn->query($sql);
        $cl_rate3=mysqli_num_rows($result);
    

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`clean_rate`='4.0'||`clean_rate`='4.5') ";
        $result = $conn->query($sql);
        $cl_rate4=mysqli_num_rows($result);
    

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and `clean_rate`='5.0' ";
        $result = $conn->query($sql);
        $cl_rate5=mysqli_num_rows($result);



        /*---------------decor rate -----------------*/
        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`decor_rate`='1.0'||`decor_rate`='1.5') ";
        $result = $conn->query($sql);
        $dr_rate1=mysqli_num_rows($result);
    

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`decor_rate`='2.0'||`decor_rate`='2.5') ";
        $result = $conn->query($sql);
        $dr_rate2=mysqli_num_rows($result);
        

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`decor_rate`='3.0'||`decor_rate`='3.5') ";
        $result = $conn->query($sql);
        $dr_rate3=mysqli_num_rows($result);
    

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`decor_rate`='4.0'||`decor_rate`='4.5') ";
        $result = $conn->query($sql);
        $dr_rate4=mysqli_num_rows($result);
    

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and `decor_rate`='5.0' ";
        $result = $conn->query($sql);
        $dr_rate5=mysqli_num_rows($result);


        /*---------------quite rate -----------------*/
        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`quite_rate`='1.0'||`quite_rate`='1.5') ";
        $result = $conn->query($sql);
        $qt_rate1=mysqli_num_rows($result);
    

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`quite_rate`='2.0'||`quite_rate`='2.5') ";
        $result = $conn->query($sql);
        $qt_rate2=mysqli_num_rows($result);
        

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`quite_rate`='3.0'||`quite_rate`='3.5') ";
        $result = $conn->query($sql);
        $qt_rate3=mysqli_num_rows($result);
    

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`quite_rate`='4.0'||`quite_rate`='4.5') ";
        $result = $conn->query($sql);
        $qt_rate4=mysqli_num_rows($result);
    

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and `quite_rate`='5.0' ";
        $result = $conn->query($sql);
        $qt_rate5=mysqli_num_rows($result);



        /*---------------service rate -----------------*/
        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`service_rate`='1.0'||`service_rate`='1.5') ";
        $result = $conn->query($sql);
        $sr_rate1=mysqli_num_rows($result);
    

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`service_rate`='2.0'||`service_rate`='2.5') ";
        $result = $conn->query($sql);
        $sr_rate2=mysqli_num_rows($result);
        

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`service_rate`='3.0'||`service_rate`='3.5') ";
        $result = $conn->query($sql);
        $sr_rate3=mysqli_num_rows($result);
    

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`service_rate`='4.0'||`service_rate`='4.5') ";
        $result = $conn->query($sql);
        $sr_rate4=mysqli_num_rows($result);
    

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and `service_rate`='5.0' ";
        $result = $conn->query($sql);
        $sr_rate5=mysqli_num_rows($result);



        /*---------------taste rate -----------------*/
        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`taste_rate`='1.0'||`taste_rate`='1.5') ";
        $result = $conn->query($sql);
        $ts_rate1=mysqli_num_rows($result);
    

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`taste_rate`='2.0'||`taste_rate`='2.5') ";
        $result = $conn->query($sql);
        $ts_rate2=mysqli_num_rows($result);
        

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`taste_rate`='3.0'||`taste_rate`='3.5') ";
        $result = $conn->query($sql);
        $ts_rate3=mysqli_num_rows($result);
    

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`taste_rate`='4.0'||`taste_rate`='4.5') ";
        $result = $conn->query($sql);
        $ts_rate4=mysqli_num_rows($result);
    

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and `taste_rate`='5.0' ";
        $result = $conn->query($sql);
        $ts_rate5=mysqli_num_rows($result);




        
        /*---------------delivery rate -----------------*/
        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`delivery_rate`='1.0'||`delivery_rate`='1.5') ";
        $result = $conn->query($sql);
        $dy_rate1=mysqli_num_rows($result);
    

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`delivery_rate`='2.0'||`delivery_rate`='2.5') ";
        $result = $conn->query($sql);
        $dy_rate2=mysqli_num_rows($result);
        

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`delivery_rate`='3.0'||`delivery_rate`='3.5') ";
        $result = $conn->query($sql);
        $dy_rate3=mysqli_num_rows($result);
    

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`delivery_rate`='4.0'||`delivery_rate`='4.5') ";
        $result = $conn->query($sql);
        $dy_rate4=mysqli_num_rows($result);
    

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and `delivery_rate`='5.0' ";
        $result = $conn->query($sql);
        $dy_rate5=mysqli_num_rows($result);



        /*---------------cost rate -----------------*/
        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`cost_rate`='1.0'||`cost_rate`='1.5') ";
        $result = $conn->query($sql);
        $ct_rate1=mysqli_num_rows($result);
    

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`cost_rate`='2.0'||`cost_rate`='2.5') ";
        $result = $conn->query($sql);
        $ct_rate2=mysqli_num_rows($result);
        

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`cost_rate`='3.0'||`cost_rate`='3.5') ";
        $result = $conn->query($sql);
        $ct_rate3=mysqli_num_rows($result);
    

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`cost_rate`='4.0'||`cost_rate`='4.5') ";
        $result = $conn->query($sql);
        $ct_rate4=mysqli_num_rows($result);
    

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and `cost_rate`='5.0' ";
        $result = $conn->query($sql);
        $ct_rate5=mysqli_num_rows($result);
        

        /*---------------emplyee rate -----------------*/
        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`employee_rate`='1.0'||`employee_rate`='1.5') ";
        $result = $conn->query($sql);
        $em_rate1=mysqli_num_rows($result);
    

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`employee_rate`='2.0'||`employee_rate`='2.5') ";
        $result = $conn->query($sql);
        $em_rate2=mysqli_num_rows($result);
        

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`employee_rate`='3.0'||`employee_rate`='3.5') ";
        $result = $conn->query($sql);
        $em_rate3=mysqli_num_rows($result);
    

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`employee_rate`='4.0'||`employee_rate`='4.5') ";
        $result = $conn->query($sql);
        $em_rate4=mysqli_num_rows($result);
    

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and `employee_rate`='5.0' ";
        $result = $conn->query($sql);
        $em_rate5=mysqli_num_rows($result);


        /*---------------resiption rate -----------------*/
        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`resiption_rate`='1.0'||`resiption_rate`='1.5') ";
        $result = $conn->query($sql);
        $rp_rate1=mysqli_num_rows($result);
    

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`resiption_rate`='2.0'||`resiption_rate`='2.5') ";
        $result = $conn->query($sql);
        $rp_rate2=mysqli_num_rows($result);
        

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`resiption_rate`='3.0'||`resiption_rate`='3.5') ";
        $result = $conn->query($sql);
        $rp_rate3=mysqli_num_rows($result);
    

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and (`resiption_rate`='4.0'||`resiption_rate`='4.5') ";
        $result = $conn->query($sql);
        $rp_rate4=mysqli_num_rows($result);
    

        $sql ="SELECT * FROM `customerinfo_v2` WHERE (DATE(date) BETWEEN '$d' AND '$n') and `resiption_rate`='5.0' ";
        $result = $conn->query($sql);
        $rp_rate5=mysqli_num_rows($result);


    

    }
    else{

        $sql ="SELECT * FROM `customerinfo_v2`";
        $result = $conn->query($sql);
        $num=mysqli_num_rows($result);
       

        if($num == 0){$num=1;}
        /*--------------location rate -------------------*/ 
        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`location_rate`='1.0'||`location_rate`='1.5') ";
        $result = $conn->query($sql);
        $loc_rate1=mysqli_num_rows($result);


        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`location_rate`='2.0'||`location_rate`='2.5') ";
        $result = $conn->query($sql);
        $loc_rate2=mysqli_num_rows($result);


        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`location_rate`='3.0'||`location_rate`='3.5') ";
        $result = $conn->query($sql);
        $loc_rate3=mysqli_num_rows($result); 
        

        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`location_rate`='4.0'||`location_rate`='4.5') ";
        $result = $conn->query($sql);
        $loc_rate4=mysqli_num_rows($result);


        $sql ="SELECT * FROM `customerinfo_v2` WHERE  `location_rate`='5.0' ";
        $result = $conn->query($sql);
        $loc_rate5=mysqli_num_rows($result);




        /*-----------------clean rate -----------------------*/

        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`clean_rate`='1.0'||`clean_rate`='1.5') ";
        $result = $conn->query($sql);
        $cl_rate1=mysqli_num_rows($result);


        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`clean_rate`='2.0'||`clean_rate`='2.5') ";
        $result = $conn->query($sql);
        $cl_rate2=mysqli_num_rows($result);


        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`clean_rate`='3.0'||`clean_rate`='3.5') ";
        $result = $conn->query($sql);
        $cl_rate3=mysqli_num_rows($result); 
        

        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`clean_rate`='4.0'||`clean_rate`='4.5') ";
        $result = $conn->query($sql);
        $cl_rate4=mysqli_num_rows($result);


        $sql ="SELECT * FROM `customerinfo_v2` WHERE  `clean_rate`='5.0' ";
        $result = $conn->query($sql);
        $cl_rate5=mysqli_num_rows($result);




        /*-----------------decor rate -----------------------*/

        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`decor_rate`='1.0'||`decor_rate`='1.5') ";
        $result = $conn->query($sql);
        $dr_rate1=mysqli_num_rows($result);


        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`decor_rate`='2.0'||`decor_rate`='2.5') ";
        $result = $conn->query($sql);
        $dr_rate2=mysqli_num_rows($result);


        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`decor_rate`='3.0'||`decor_rate`='3.5') ";
        $result = $conn->query($sql);
        $dr_rate3=mysqli_num_rows($result); 
        

        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`decor_rate`='4.0'||`decor_rate`='4.5') ";
        $result = $conn->query($sql);
        $dr_rate4=mysqli_num_rows($result);


        $sql ="SELECT * FROM `customerinfo_v2` WHERE  `decor_rate`='5.0' ";
        $result = $conn->query($sql);
        $dr_rate5=mysqli_num_rows($result);



        
        /*-----------------quite rate -----------------------*/

        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`quite_rate`='1.0'||`quite_rate`='1.5') ";
        $result = $conn->query($sql);
        $qt_rate1=mysqli_num_rows($result);


        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`quite_rate`='2.0'||`quite_rate`='2.5') ";
        $result = $conn->query($sql);
        $qt_rate2=mysqli_num_rows($result);


        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`quite_rate`='3.0'||`quite_rate`='3.5') ";
        $result = $conn->query($sql);
        $qt_rate3=mysqli_num_rows($result); 
        

        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`quite_rate`='4.0'||`quite_rate`='4.5') ";
        $result = $conn->query($sql);
        $qt_rate4=mysqli_num_rows($result);


        $sql ="SELECT * FROM `customerinfo_v2` WHERE  `quite_rate`='5.0' ";
        $result = $conn->query($sql);
        $qt_rate5=mysqli_num_rows($result);



         /*-----------------service rate -----------------------*/

         $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`service_rate`='1.0'||`service_rate`='1.5') ";
         $result = $conn->query($sql);
         $sr_rate1=mysqli_num_rows($result);
 

         $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`service_rate`='2.0'||`service_rate`='2.5') ";
         $result = $conn->query($sql);
         $sr_rate2=mysqli_num_rows($result);
 

         $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`service_rate`='3.0'||`service_rate`='3.5') ";
         $result = $conn->query($sql);
         $sr_rate3=mysqli_num_rows($result); 
         

         $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`service_rate`='4.0'||`service_rate`='4.5') ";
         $result = $conn->query($sql);
         $sr_rate4=mysqli_num_rows($result);
 

         $sql ="SELECT * FROM `customerinfo_v2` WHERE  `service_rate`='5.0' ";
         $result = $conn->query($sql);
         $sr_rate5=mysqli_num_rows($result);




        /*--------------taste rate -------------------*/ 
        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`taste_rate`='1.0'||`taste_rate`='1.5') ";
        $result = $conn->query($sql);
        $ts_rate1=mysqli_num_rows($result);


        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`taste_rate`='2.0'||`taste_rate`='2.5') ";
        $result = $conn->query($sql);
        $ts_rate2=mysqli_num_rows($result);


        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`taste_rate`='3.0'||`taste_rate`='3.5') ";
        $result = $conn->query($sql);
        $ts_rate3=mysqli_num_rows($result); 
        

        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`taste_rate`='4.0'||`taste_rate`='4.5') ";
        $result = $conn->query($sql);
        $ts_rate4=mysqli_num_rows($result);


        $sql ="SELECT * FROM `customerinfo_v2` WHERE  `taste_rate`='5.0' ";
        $result = $conn->query($sql);
        $ts_rate5=mysqli_num_rows($result);




        /*--------------delivery rate -------------------*/ 
        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`delivery_rate`='1.0'||`delivery_rate`='1.5') ";
        $result = $conn->query($sql);
        $dy_rate1=mysqli_num_rows($result);


        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`delivery_rate`='2.0'||`delivery_rate`='2.5') ";
        $result = $conn->query($sql);
        $dy_rate2=mysqli_num_rows($result);


        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`delivery_rate`='3.0'||`delivery_rate`='3.5') ";
        $result = $conn->query($sql);
        $dy_rate3=mysqli_num_rows($result); 
        

        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`delivery_rate`='4.0'||`delivery_rate`='4.5') ";
        $result = $conn->query($sql);
        $dy_rate4=mysqli_num_rows($result);


        $sql ="SELECT * FROM `customerinfo_v2` WHERE  `delivery_rate`='5.0' ";
        $result = $conn->query($sql);
        $dy_rate5=mysqli_num_rows($result);



        /*--------------cost rate -------------------*/ 
        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`cost_rate`='1.0'||`cost_rate`='1.5') ";
        $result = $conn->query($sql);
        $ct_rate1=mysqli_num_rows($result);


        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`cost_rate`='2.0'||`cost_rate`='2.5') ";
        $result = $conn->query($sql);
        $ct_rate2=mysqli_num_rows($result);


        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`cost_rate`='3.0'||`cost_rate`='3.5') ";
        $result = $conn->query($sql);
        $ct_rate3=mysqli_num_rows($result); 
        

        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`cost_rate`='4.0'||`cost_rate`='4.5') ";
        $result = $conn->query($sql);
        $ct_rate4=mysqli_num_rows($result);


        $sql ="SELECT * FROM `customerinfo_v2` WHERE  `cost_rate`='5.0' ";
        $result = $conn->query($sql);
        $ct_rate5=mysqli_num_rows($result);


        /*--------------employee rate -------------------*/ 
        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`employee_rate`='1.0'||`employee_rate`='1.5') ";
        $result = $conn->query($sql);
        $em_rate1=mysqli_num_rows($result);


        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`employee_rate`='2.0'||`employee_rate`='2.5') ";
        $result = $conn->query($sql);
        $em_rate2=mysqli_num_rows($result);


        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`employee_rate`='3.0'||`employee_rate`='3.5') ";
        $result = $conn->query($sql);
        $em_rate3=mysqli_num_rows($result); 
        

        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`employee_rate`='4.0'||`employee_rate`='4.5') ";
        $result = $conn->query($sql);
        $em_rate4=mysqli_num_rows($result);


        $sql ="SELECT * FROM `customerinfo_v2` WHERE  `employee_rate`='5.0' ";
        $result = $conn->query($sql);
        $em_rate5=mysqli_num_rows($result);



        /*--------------resiption rate -------------------*/ 
        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`resiption_rate`='1.0'||`resiption_rate`='1.5') ";
        $result = $conn->query($sql);
        $rp_rate1=mysqli_num_rows($result);


        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`resiption_rate`='2.0'||`resiption_rate`='2.5') ";
        $result = $conn->query($sql);
        $rp_rate2=mysqli_num_rows($result);


        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`resiption_rate`='3.0'||`resiption_rate`='3.5') ";
        $result = $conn->query($sql);
        $rp_rate3=mysqli_num_rows($result); 
        

        $sql ="SELECT * FROM `customerinfo_v2` WHERE  (`resiption_rate`='4.0'||`resiption_rate`='4.5') ";
        $result = $conn->query($sql);
        $rp_rate4=mysqli_num_rows($result);


        $sql ="SELECT * FROM `customerinfo_v2` WHERE  `resiption_rate`='5.0' ";
        $result = $conn->query($sql);
        $rp_rate5=mysqli_num_rows($result);





        

    }
   
    
?>


 <!---------------- location rate ----------------------->

<div class="diveva">
<hr style="border:3px solid #000">
<div style="direction:rtl">
<span class="heading">تقييم الموقع</span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star"></span>
<p> العدد الكلي </p> <p><?php echo($num)?></p>
<hr style="border:3px solid #f1f1f1">
</div>



<div class="row">
 
    <div class="side">
        <div>5 نجوم</div>
    </div>

    <div class="middle">
        <div class="bar-container">
        <div class="bar-5" id="test" style='width: <?php echo $loc_rate5/$num *100?>%'></div>
        </div>
    </div>

    <div class="side right">
        <div><?php echo $loc_rate5/$num *100?>%</div>
    </div>


        <div class="side">
            <div>4 نجوم</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-4" style='width: <?php echo $loc_rate4/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $loc_rate4/$num *100?>%</div>
        </div>


        <div class="side">
            <div>3 نجوم</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-3" style='width: <?php echo $loc_rate3/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $loc_rate3/$num *100?>%</div>
        </div>


        <div class="side">
            <div>2 نجمة</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-2" style='width: <?php echo $loc_rate2/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $loc_rate2/$num *100?>%</div>
        </div>



        <div class="side">
            <div>1 نجمة</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-1" style='width: <?php echo $loc_rate1/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
        <div><?php echo($loc_rate1/$num *100)?>%</div>
        </div>
</div>

</div>




<!---------------- clean rate ----------------------->

<div class="diveva">
<hr style="border:3px solid #000">
<div style="direction:rtl">
<span class="heading">تقييم النظافة</span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star"></span>
<p> العدد الكلي </p> <p><?php echo($num)?></p>
<hr style="border:3px solid #f1f1f1">
</div>



<div class="row">
 
    <div class="side">
        <div>5 نجوم</div>
    </div>

    <div class="middle">
        <div class="bar-container">
        <div class="bar-5" id="test" style='width: <?php echo $cl_rate5/$num *100?>%'></div>
        </div>
    </div>

    <div class="side right">
        <div><?php echo $cl_rate5/$num *100?>%</div>
    </div>


        <div class="side">
            <div>4 نجوم</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-4" style='width: <?php echo $cl_rate4/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $cl_rate4/$num *100?>%</div>
        </div>


        <div class="side">
            <div>3 نجوم</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-3" style='width: <?php echo $cl_rate3/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $cl_rate3/$num *100?>%</div>
        </div>


        <div class="side">
            <div>2 نجمة</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-2" style='width: <?php echo $cl_rate2/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $cl_rate2/$num *100?>%</div>
        </div>



        <div class="side">
            <div>1 نجمة</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-1" style='width: <?php echo $cl_rate1/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
        <div><?php echo($cl_rate1/$num *100)?>%</div>
        </div>
</div>

</div>




<!---------------- decor rate ----------------------->

<div class="diveva">
<hr style="border:3px solid #000">
<div style="direction:rtl">
<span class="heading">تقييم الديكور</span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star"></span>
<p> العدد الكلي </p> <p><?php echo($num)?></p>
<hr style="border:3px solid #f1f1f1">
</div>



<div class="row">
 
    <div class="side">
        <div>5 نجوم</div>
    </div>

    <div class="middle">
        <div class="bar-container">
        <div class="bar-5" id="test" style='width: <?php echo $dr_rate5/$num *100?>%'></div>
        </div>
    </div>

    <div class="side right">
        <div><?php echo $dr_rate5/$num *100?>%</div>
    </div>


        <div class="side">
            <div>4 نجوم</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-4" style='width: <?php echo $dr_rate4/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $dr_rate4/$num *100?>%</div>
        </div>


        <div class="side">
            <div>3 نجوم</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-3" style='width: <?php echo $dr_rate3/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $dr_rate3/$num *100?>%</div>
        </div>


        <div class="side">
            <div>2 نجمة</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-2" style='width: <?php echo $dr_rate2/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $dr_rate2/$num *100?>%</div>
        </div>



        <div class="side">
            <div>1 نجمة</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-1" style='width: <?php echo $dr_rate1/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
        <div><?php echo($dr_rate1/$num *100)?>%</div>
        </div>
</div>

</div>






<!---------------- quite rate ----------------------->

<div class="diveva">
<hr style="border:3px solid #000">
<div style="direction:rtl">
<span class="heading">تقيمم الهدوء </span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star"></span>
<p> العدد الكلي </p> <p><?php echo($num)?></p>
<hr style="border:3px solid #f1f1f1">
</div>



<div class="row">
 
    <div class="side">
        <div>5 نجوم</div>
    </div>

    <div class="middle">
        <div class="bar-container">
        <div class="bar-5" id="test" style='width: <?php echo $qt_rate5/$num *100?>%'></div>
        </div>
    </div>

    <div class="side right">
        <div><?php echo $qt_rate5/$num *100?>%</div>
    </div>


        <div class="side">
            <div>4 نجوم</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-4" style='width: <?php echo $qt_rate4/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $qt_rate4/$num *100?>%</div>
        </div>


        <div class="side">
            <div>3 نجوم</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-3" style='width: <?php echo $qt_rate3/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $qt_rate3/$num *100?>%</div>
        </div>


        <div class="side">
            <div>2 نجمة</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-2" style='width: <?php echo $qt_rate2/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $qt_rate2/$num *100?>%</div>
        </div>

        <div class="side">
            <div>1 نجمة</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-1" style='width: <?php echo $qt_rate1/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
        <div><?php echo($qt_rate1/$num *100)?>%</div>
        </div>
</div>

</div>



<!---------------- service rate ----------------------->

<div class="diveva">
<hr style="border:3px solid #000">
<div style="direction:rtl">
<span class="heading">تقييم الخدمة</span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star"></span>
<p> العدد الكلي </p> <p><?php echo($num)?></p>
<hr style="border:3px solid #f1f1f1">
</div>



<div class="row">
 
    <div class="side">
        <div>5 نجوم</div>
    </div>

    <div class="middle">
        <div class="bar-container">
        <div class="bar-5" id="test" style='width: <?php echo $sr_rate5/$num *100?>%'></div>
        </div>
    </div>

    <div class="side right">
        <div><?php echo $sr_rate5/$num *100?>%</div>
    </div>


        <div class="side">
            <div>4 نجوم</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-4" style='width: <?php echo $sr_rate4/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $sr_rate4/$num *100?>%</div>
        </div>


        <div class="side">
            <div>3 نجوم</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-3" style='width: <?php echo $sr_rate3/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $sr_rate3/$num *100?>%</div>
        </div>


        <div class="side">
            <div>2 نجمة</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-2" style='width: <?php echo $sr_rate2/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $sr_rate2/$num *100?>%</div>
        </div>

        <div class="side">
            <div>1 نجمة</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-1" style='width: <?php echo $sr_rate1/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
        <div><?php echo($sr_rate1/$num *100)?>%</div>
        </div>
</div>

</div>




<!---------------- service rate ----------------------->

<div class="diveva">
<hr style="border:3px solid #000">
<div style="direction:rtl">
<span class="heading">تقييم الطعم </span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star"></span>
<p> العدد الكلي </p> <p><?php echo($num)?></p>
<hr style="border:3px solid #f1f1f1">
</div>



<div class="row">
 
    <div class="side">
        <div>5 نجوم</div>
    </div>

    <div class="middle">
        <div class="bar-container">
        <div class="bar-5" id="test" style='width: <?php echo $ts_rate5/$num *100?>%'></div>
        </div>
    </div>

    <div class="side right">
        <div><?php echo $ts_rate5/$num *100?>%</div>
    </div>


        <div class="side">
            <div>4 نجوم</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-4" style='width: <?php echo $ts_rate4/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $ts_rate4/$num *100?>%</div>
        </div>


        <div class="side">
            <div>3 نجوم</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-3" style='width: <?php echo $ts_rate3/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $ts_rate3/$num *100?>%</div>
        </div>


        <div class="side">
            <div>2 نجمة</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-2" style='width: <?php echo $ts_rate2/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $ts_rate2/$num *100?>%</div>
        </div>

        <div class="side">
            <div>1 نجمة</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-1" style='width: <?php echo $ts_rate1/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
        <div><?php echo($ts_rate1/$num *100)?>%</div>
        </div>
</div>

</div>




<!---------------- delivery rate ----------------------->

<div class="diveva">
<hr style="border:3px solid #000">
<div style="direction:rtl">
<span class="heading">تقييم سرعة الخدمة  </span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star"></span>
<p> العدد الكلي </p> <p><?php echo($num)?></p>
<hr style="border:3px solid #f1f1f1">
</div>



<div class="row">
 
    <div class="side">
        <div>5 نجوم</div>
    </div>

    <div class="middle">
        <div class="bar-container">
        <div class="bar-5" id="test" style='width: <?php echo $dy_rate5/$num *100?>%'></div>
        </div>
    </div>

    <div class="side right">
        <div><?php echo $dy_rate5/$num *100?>%</div>
    </div>


        <div class="side">
            <div>4 نجوم</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-4" style='width: <?php echo $dy_rate4/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $dy_rate4/$num *100?>%</div>
        </div>


        <div class="side">
            <div>3 نجوم</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-3" style='width: <?php echo $dy_rate3/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $dy_rate3/$num *100?>%</div>
        </div>


        <div class="side">
            <div>2 نجمة</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-2" style='width: <?php echo $dy_rate2/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $dy_rate2/$num *100?>%</div>
        </div>

        <div class="side">
            <div>1 نجمة</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-1" style='width: <?php echo $dy_rate1/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
        <div><?php echo($dy_rate1/$num *100)?>%</div>
        </div>
</div>

</div>






<!---------------- cost rate ----------------------->

<div class="diveva">
<hr style="border:3px solid #000">
<div style="direction:rtl">
<span class="heading">تقييم السعر </span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star"></span>
<p> العدد الكلي </p> <p><?php echo($num)?></p>
<hr style="border:3px solid #f1f1f1">
</div>



<div class="row">
 
    <div class="side">
        <div>5 نجوم</div>
    </div>

    <div class="middle">
        <div class="bar-container">
        <div class="bar-5" id="test" style='width: <?php echo $ct_rate5/$num *100?>%'></div>
        </div>
    </div>

    <div class="side right">
        <div><?php echo $ct_rate5/$num *100?>%</div>
    </div>


        <div class="side">
            <div>4 نجوم</div>
        </div>
        <div class="middle"> 
            <div class="bar-container">
            <div class="bar-4" style='width: <?php echo $ct_rate4/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $ct_rate4/$num *100?>%</div>
        </div>


        <div class="side">
            <div>3 نجوم</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-3" style='width: <?php echo $ct_rate3/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $ct_rate3/$num *100?>%</div>
        </div>


        <div class="side">
            <div>2 نجمة</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-2" style='width: <?php echo $ct_rate2/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $ct_rate2/$num *100?>%</div>
        </div>

        <div class="side">
            <div>1 نجمة</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-1" style='width: <?php echo $ct_rate1/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
        <div><?php echo($ct_rate1/$num *100)?>%</div>
        </div>
</div>

</div>





<!---------------- employee rate ----------------------->

<div class="diveva">
<hr style="border:3px solid #000">
<div style="direction:rtl">
<span class="heading">تقييم الموظفين </span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star"></span>
<p> العدد الكلي </p> <p><?php echo($num)?></p>
<hr style="border:3px solid #f1f1f1">
</div>



<div class="row">
 
    <div class="side">
        <div>5 نجوم</div>
    </div>

    <div class="middle">
        <div class="bar-container">
        <div class="bar-5" id="test" style='width: <?php echo $em_rate5/$num *100?>%'></div>
        </div>
    </div>

    <div class="side right">
        <div><?php echo $em_rate5/$num *100?>%</div>
    </div>


        <div class="side">
            <div>4 نجوم</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-4" style='width: <?php echo $em_rate4/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $em_rate4/$num *100?>%</div>
        </div>


        <div class="side">
            <div>3 نجوم</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-3" style='width: <?php echo $em_rate3/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $em_rate3/$num *100?>%</div>
        </div>


        <div class="side">
            <div>2 نجمة</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-2" style='width: <?php echo $em_rate2/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $em_rate2/$num *100?>%</div>
        </div>

        <div class="side">
            <div>1 نجمة</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-1" style='width: <?php echo $em_rate1/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
        <div><?php echo($em_rate1/$num *100)?>%</div>
        </div>
</div>

</div>



<!---------------- resiption rate ----------------------->

<div class="diveva">
<hr style="border:3px solid #000">
<div style="direction:rtl">
<span class="heading">تقييم الاستقبال </span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star"></span>
<p> العدد الكلي </p> <p><?php echo($num)?></p>
<hr style="border:3px solid #f1f1f1">
</div>



<div class="row">
 
    <div class="side">
        <div>5 نجوم</div>
    </div>

    <div class="middle">
        <div class="bar-container">
        <div class="bar-5" id="test" style='width: <?php echo $rp_rate5/$num *100?>%'></div>
        </div>
    </div>

    <div class="side right">
        <div><?php echo $rp_rate5/$num *100?>%</div>
    </div>


        <div class="side">
            <div>4 نجوم</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-4" style='width: <?php echo $rp_rate4/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $rp_rate4/$num *100?>%</div>
        </div>


        <div class="side">
            <div>3 نجوم</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-3" style='width: <?php echo $rp_rate3/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $rp_rate3/$num *100?>%</div>
        </div>


        <div class="side">
            <div>2 نجمة</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-2" style='width: <?php echo $rp_rate2/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
            <div><?php echo $rp_rate2/$num *100?>%</div>
        </div>

        <div class="side">
            <div>1 نجمة</div>
        </div>
        <div class="middle">
            <div class="bar-container">
            <div class="bar-1" style='width: <?php echo $rp_rate1/$num *100?>%'></div>
            </div>
        </div>
        <div class="side right">
        <div><?php echo($rp_rate1/$num *100)?>%</div>
        </div>
</div>

</div>









</body>
</html>


