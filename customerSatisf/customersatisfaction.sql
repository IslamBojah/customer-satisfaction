-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 07, 2018 at 08:24 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `customersatisfaction`
--

-- --------------------------------------------------------

--
-- Table structure for table `customerinfo`
--

CREATE TABLE `customerinfo` (
  `id` int(255) NOT NULL,
  `customer_name` varchar(50) NOT NULL,
  `customer_phone` varchar(10) NOT NULL,
  `study_level` varchar(15) NOT NULL,
  `customer_job` varchar(50) NOT NULL,
  `customer_email` varchar(50) NOT NULL,
  `location_rate` varchar(10) NOT NULL,
  `clean_rate` varchar(10) NOT NULL,
  `decor_rate` varchar(10) NOT NULL,
  `quite_rate` varchar(10) NOT NULL,
  `service_rate` varchar(10) NOT NULL,
  `taste_rate` varchar(10) NOT NULL,
  `delivery_rate` varchar(10) NOT NULL,
  `cost_rate` varchar(10) NOT NULL,
  `employee_rate` varchar(10) NOT NULL,
  `resiption_rate` varchar(10) NOT NULL,
  `q1` varchar(30) NOT NULL,
  `q2` varchar(10) NOT NULL,
  `q3` varchar(10) NOT NULL,
  `q4` varchar(10) NOT NULL,
  `prefer_resturant` varchar(30) NOT NULL,
  `suggestion` text NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customerinfo`
--

INSERT INTO `customerinfo` (`id`, `customer_name`, `customer_phone`, `study_level`, `customer_job`, `customer_email`, `location_rate`, `clean_rate`, `decor_rate`, `quite_rate`, `service_rate`, `taste_rate`, `delivery_rate`, `cost_rate`, `employee_rate`, `resiption_rate`, `q1`, `q2`, `q3`, `q4`, `prefer_resturant`, `suggestion`, `date`) VALUES
(1, 'تجربة', '5855565658', '', 'تجربة', 'email@ex.com', '3.0', '4.0', '3.0', '5.0', '4.0', '3.0', '3.0', '4.0', '4.0', '3.0', '', '', '', '', '', '', '2018-11-01'),
(2, 'تجربة ', '7848549579', '', 'تجربة ', 'email@ex.com', '4.0', '4.0', '4.0', '3.0', '4.0', '4.0', '4.0', '3.0', '4.0', '4.0', '', '', '', '', '', '', '2018-11-01'),
(3, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-11-01'),
(4, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-11-01');

-- --------------------------------------------------------

--
-- Table structure for table `customerinfo_v2`
--

CREATE TABLE `customerinfo_v2` (
  `id` int(255) NOT NULL,
  `customer_name` varchar(50) NOT NULL,
  `customer_phone` varchar(20) NOT NULL,
  `customer_job` varchar(50) NOT NULL,
  `customer_email` varchar(50) NOT NULL,
  `location_rate` varchar(10) NOT NULL,
  `clean_rate` varchar(10) NOT NULL,
  `decor_rate` varchar(10) NOT NULL,
  `quite_rate` varchar(10) NOT NULL,
  `service_rate` varchar(10) NOT NULL,
  `taste_rate` varchar(10) NOT NULL,
  `delivery_rate` varchar(10) NOT NULL,
  `cost_rate` varchar(10) NOT NULL,
  `employee_rate` varchar(10) NOT NULL,
  `resiption_rate` varchar(10) NOT NULL,
  `suggestion` text NOT NULL,
  `date` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customerinfo_v2`
--

INSERT INTO `customerinfo_v2` (`id`, `customer_name`, `customer_phone`, `customer_job`, `customer_email`, `location_rate`, `clean_rate`, `decor_rate`, `quite_rate`, `service_rate`, `taste_rate`, `delivery_rate`, `cost_rate`, `employee_rate`, `resiption_rate`, `suggestion`, `date`) VALUES
(1, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-11-01'),
(2, 'تجربة ١', '0006858688', 'تجربة ١', 'email@ex.com', '', '4.0', '4.0', '3.0', '5.0', '4.0', '4.0', '5.0', '5.0', '5.0', 'لا يوجد', '2018-11-01');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(255) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `usertype` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `usertype`) VALUES
(3, 'admin', 'admin', 1),
(4, 'test', 'test', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customerinfo`
--
ALTER TABLE `customerinfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customerinfo_v2`
--
ALTER TABLE `customerinfo_v2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customerinfo`
--
ALTER TABLE `customerinfo`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `customerinfo_v2`
--
ALTER TABLE `customerinfo_v2`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
